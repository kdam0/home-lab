This part of the repo is responsible for provisioning our
home-lab services using Ansible on top of the infrastructure already 
created via Terraform.

The basic pattern is to confiure the service, and then add the upstream host 
to our reverse proxy (Caddy) configuration.

![architecture image](archi.png)

# TODOs (in-progress)
1. reverse-proxy init
1. minio user,bucket init
1. static sites using minio bucket

# Done
1. Adding service to reverse-proxy (Caddy) via api
1. Media Downloader with Transmission, and `*arr` services
1. Plex
1. Password Manager
1. Nextcloud
1. Metrics monitoring  

---

# Pre-reqs
* `pip`3 - `sudo apt install python3 python3-pip`
* `ansible` - `pip install --user ansible`

# Execution
1. (First Time Only) Fork this repo, if you haven't already.
1. (FTO) Make sure that your [`hosts`](./hosts) have been created in the [Terraform](../terraform) section.
1. (FTO) Edit `group_vars/all/vault.example` to your liking.
```bash
echo "YourSecretPassword" > vaultkey
ansible-vault encrypt --output group_vars/all/vault --encrypt-vault-id vaultkey group_vars/all/vault.example
git checkout group_vars/all/vault.example
```
1. View/edit your encrypted vault:
```bash
ansible-vault view/edit --vault-id vaultkey group_vars/all/vault
```
1. (FTO) Run the `init.yaml` playbook to install a service
```bash
ansible-playbook -u youruser -i hosts --vault-id vaultkey playbooks/init.yaml --tags=[app name]
```
1. Use another playbook to modify a service (not yet implemented but planned)
```bash
ansible-playbook -u youruser -i hosts --vault-id vaultkey playbooks/... --tags=[...]
```

# Ansible Roles
You can think of each Role being an "app".

Each Role has a `setup.yaml` task file responsible for installing the app.
It may or may not have other task files depending on what other functionality this Role
supports for this app.

The `main.yaml` file in the Role determines the flow logic for which task files
are getting run based on Tags for other functionalities (see Tags section below).


# Artifacts
The directory `playbooks/artifacts/` is a place where we save the output for some of the roles.

This not only serves the purpose to tracking important state info for our configurations,
but it also allows us to use some of this info in other parts of Ansible for cleaner automation.

> Never commit this dir to git. It contains sensitive info! Its already configured to 
be ignored by git.


# Ansible Tags
Setting up the app is normally done using the Role's name as the tag, 
i.e. `--tags <role name>`.

Any subsequent tasks required beyond setting up the app, will have a different 
tag associated for those tasks (along with a different playbook to trigger them).
For example, the MinIO Role will support adding a new user+bucket under the tag `add_minio_user`:

```bash
ansible-playbook ... playbooks/add_user.yaml --tags add_minio_user
```
