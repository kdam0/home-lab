{
  description = "My Home Lab project";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    devenv.url = "github:cachix/devenv";
  };

  outputs = inputs@{ flake-parts, nixpkgs, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.devenv.flakeModule
      ];
      systems = nixpkgs.lib.systems.flakeExposed;
      perSystem = { config, self', inputs', pkgs, system, ... }: {

        packages.default = pkgs.hello;

        devenv.shells.ansible = {
          packages = [ config.packages.default ];
          languages.python.enable = true;
          languages.python.poetry.enable = true;
          languages.python.poetry.activate.enable = true;
          enterShell = ''
            echo this is my ansible project shell.
          '';
        };

        devenv.shells.terraform = {
          packages = [ config.packages.default ];
          languages.opentofu.enable = true;
          enterShell = ''
            echo this is a terraform project shell.
          '';
        };
      };
    };
}
