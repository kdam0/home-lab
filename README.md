# My Home-Lab as Code

Welcome to my home!...lab.

This is my personal project to setup all my `r/self-hosted` services
in my home-lab.

The goal of this repo is to be able to survive a dooms-day scenario where all 
of my hosts die, and I can use to use this repo to be able to re-create it all
automatically.

I use Proxox VE 8, which comes with a very recent kernel and thus good HW support
for my hypervisor host - a Framework Laptop. Yes, you read that right. 
Before you start calling me a sadist, the main reason (other than money) I went with 
this is that frankly, I have no better use for this machine. It just sat around 
powered down 99% of the time, and only got used for IaC stuff like this, or 
personal blog posts. This machine is clearly overkill for these tasks.

If we are to get technical - this is a git repo after all - 
it (the mainboard) is running in its own case, in "standalone" mode aka. as a poor man's "server".
You can read more about this in my [blog](https://kumardamani.net/post/framework-server/).

Believe it or not, this is actually a big upgrade for me. Prior to this, all of my 
services were running off my NAS - Rockchip RK3399, not an i7-1165G7. 
And I quickly became aware of its limitation when trying to run Collabora Online one day...

Anyways, now I can finally off-load most of "compute" work to better suited HW, and let the 
NAS do its NAS thing - provide block storage via NFS to this "compute" server.

# What is NOT covered in this repo?
* The NAS. I already covered this [here](https://gitlab.com/kdam0/vps#nas-setup).
* The Proxmox install itself, along with networking config of my lab. VPN, VLANs etc. 
I go over this in my [blog](https://kumardamani.net/post/framework-server/). 
For now, its all manual.

# What IS covered in this repo?
* Creating the container/VM infrastructure with [Terraform](https://www.terraform.io/). 
Refer to [this](./terraform/).
* Setting up the application services I self-host with [Ansible](https://www.ansible.com/). 
Refer to [this](./ansible/).
