This part of the repo is responsible for creating 
our home-lab infrastructure using Terraform.

# First time
```
terraform init
cp con.env.example con.env
```
Modify  `con.env` with your Proxmox access creds.

> Due to [this](https://github.com/bpg/terraform-provider-proxmox/issues/344) issue, 
we cannot use the recommended approach dedicated TF user's API access.

# Usual flow
```
source con.env

terraform plan -out=tfplan
#terraform plan -target="proxmox_lxc.testvm" -out=tfplan

terraform apply "tfplan"
```

# Teardown
```
terraform destroy
terraform destroy -target="proxmox_lxc.testvm"
```

# A note about GPU pass-through
At the moment, this provider does not support passing through the Host's GPU or iGPU 
to the guest VM/LXC. So, if you have Guests that require this, you need to do it manually
by adding the following config on Proxmox `/etc/pve/lxc/<id of your container>.conf`:
```
lxc.cgroup2.devices.allow: c 226:0 rwm
lxc.cgroup2.devices.allow: c 226:128 rwm
lxc.cgroup2.devices.allow: c 29:0 rwm
lxc.mount.entry: /dev/dri dev/dri none bind,optional,create=dir
lxc.mount.entry: /dev/dri/renderD128 dev/renderD128 none bind,optional,create=file
```

> I do this for my Nextcloud, and Plex containers.
