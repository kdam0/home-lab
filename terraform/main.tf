resource "proxmox_lxc" "minio" {
  target_node  = "hesh"
  hostname     = "minio"
  ostemplate   = "local:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"
  password     = "${var.minio_password}"
  unprivileged = false
  cores        = 1
  cpulimit     = 2
  memory       = 2048
  start        = true
  onboot       = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM9ONfhaHpY0bSk3o81H/eEp+UxQ9RCszJ7p3f+rvcpH kdam0@art-sr
  EOT

  rootfs {
    storage = "local-lvm"
    size    = "3G"
  }

  features {
    mount   = "nfs;"
  }

  network {
    name   = "eth0"
    bridge = "vmbr1"
    tag    = 30
    ip     = "dhcp"
    mtu    = "1420"
  }
}

resource "proxmox_lxc" "sites" {
  target_node  = "hesh"
  hostname     = "sites"
  ostemplate   = "local:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"
  password     = "${var.sites_password}"
  unprivileged = true
  cores        = 1
  cpulimit     = 1
  memory       = 1024
  start        = true
  onboot       = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM9ONfhaHpY0bSk3o81H/eEp+UxQ9RCszJ7p3f+rvcpH kdam0@art-sr
  EOT

  rootfs {
    storage = "local-lvm"
    size    = "3G"
  }

  features {
    fuse    = true
  }

  network {
    name   = "eth0"
    bridge = "vmbr1"
    tag    = 30
    ip     = "dhcp"
    mtu    = "1420"
  }
}

resource "proxmox_lxc" "rproxy" {
  target_node  = "hesh"
  hostname     = "rproxy"
  ostemplate   = "local:vztmpl/alpine-3.18-default_20230607_amd64.tar.xz"
  password     = "${var.rproxy_password}"
  unprivileged = true
  cores        = 1
  cpulimit     = 1
  memory       = 1024
  start        = true
  onboot       = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM9ONfhaHpY0bSk3o81H/eEp+UxQ9RCszJ7p3f+rvcpH kdam0@art-sr
  EOT

  rootfs {
    storage = "local-lvm"
    size    = "2G"
  }

  network {
    name   = "eth0"
    bridge = "vmbr1"
    tag    = 30
    ip     = "dhcp"
    mtu    = "1420"
  }
}

resource "proxmox_lxc" "monitoring" {
  target_node  = "hesh"
  hostname     = "monitoring"
  ostemplate   = "local:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"
  password     = "${var.monitoring_password}"
  unprivileged = false
  cores        = 1
  cpulimit     = 2
  memory       = 1024
  start        = true
  onboot       = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM9ONfhaHpY0bSk3o81H/eEp+UxQ9RCszJ7p3f+rvcpH kdam0@art-sr
  EOT

  rootfs {
    storage = "local-lvm"
    size    = "10G"
  }

  features {
    nesting = true
  }

  network {
    name   = "eth0"
    bridge = "vmbr1"
    tag    = 30
    ip     = "dhcp"
    mtu    = "1420"
  }
}

resource "proxmox_lxc" "nc_kumar" {
  target_node  = "hesh"
  hostname     = "nc-kumar"
  ostemplate   = "local:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"
  password     = "${var.nc_kumar_password}"
  unprivileged = false
  cores        = 8
  cpulimit     = 8
  memory       = 12288
  start        = true
  onboot       = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM9ONfhaHpY0bSk3o81H/eEp+UxQ9RCszJ7p3f+rvcpH kdam0@art-sr
  EOT

  rootfs {
    storage = "local-lvm"
    size    = "15G"
  }

  features {
    nesting = true
    fuse    = true
    mount   = "nfs;"
  }

  network {
    name   = "eth0"
    bridge = "vmbr1"
    tag    = 30
    ip     = "dhcp"
    mtu    = "1420"
  }
}

resource "proxmox_lxc" "plex" {
  target_node  = "hesh"
  hostname     = "plex"
  ostemplate   = "local:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"
  password     = "${var.plex_password}"
  unprivileged = false
  cores        = 8
  cpulimit     = 8
  memory       = 4096
  start        = true
  onboot       = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM9ONfhaHpY0bSk3o81H/eEp+UxQ9RCszJ7p3f+rvcpH kdam0@art-sr
  EOT

  rootfs {
    storage = "local-lvm"
    size    = "100G"
  }

  features {
    nesting = true
    fuse    = true
    mount   = "nfs;"
  }

  network {
    name   = "eth0"
    bridge = "vmbr1"
    tag    = 30
    ip     = "dhcp"
    mtu    = "1420"
  }
}

resource "proxmox_lxc" "vault" {
  target_node  = "hesh"
  hostname     = "vault"
  ostemplate   = "local:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"
  password     = "${var.vault_password}"
  unprivileged = true
  cores        = 1
  cpulimit     = 2
  memory       = 1024
  start        = true
  onboot       = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM9ONfhaHpY0bSk3o81H/eEp+UxQ9RCszJ7p3f+rvcpH kdam0@art-sr
  EOT

  rootfs {
    storage = "local-lvm"
    size    = "10G"
  }

  features {
    nesting = true
  }

  network {
    name   = "eth0"
    bridge = "vmbr1"
    tag    = 30
    ip     = "dhcp"
    mtu    = "1420"
  }
}

resource "proxmox_lxc" "torrents" {
  target_node  = "hesh"
  hostname     = "torrents"
  ostemplate   = "local:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"
  password     = "${var.torrents_password}"
  unprivileged = false
  cores        = 2
  cpulimit     = 2
  memory       = 2048
  start        = true
  onboot       = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM9ONfhaHpY0bSk3o81H/eEp+UxQ9RCszJ7p3f+rvcpH kdam0@art-sr
  EOT

  rootfs {
    storage = "local-lvm"
    size    = "100G"
  }

  features {
    nesting = true
    fuse    = true
    mount   = "nfs;"
  }

  network {
    name   = "eth0"
    bridge = "vmbr1"
    tag    = 30
    ip     = "dhcp"
    mtu    = "1420"
  }
}

resource "proxmox_lxc" "med_dl" {
  target_node  = "hesh"
  hostname     = "med-dl"
  ostemplate   = "local:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"
  password     = "${var.med_dl_password}"
  unprivileged = false
  cores        = 4
  cpulimit     = 4
  memory       = 2048
  start        = true
  onboot       = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM9ONfhaHpY0bSk3o81H/eEp+UxQ9RCszJ7p3f+rvcpH kdam0@art-sr
  EOT

  rootfs {
    storage = "local-lvm"
    size    = "10G"
  }

  features {
    nesting = true
    fuse    = true
    mount   = "nfs;"
  }

  network {
    name   = "eth0"
    bridge = "vmbr1"
    tag    = 30
    ip     = "dhcp"
    mtu    = "1420"
  }
}
