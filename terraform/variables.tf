variable "minio_password" {
  type        = string
  sensitive   = true
  description = "Secret key for minio"
}
variable "sites_password" {
  type        = string
  sensitive   = true
  description = "Secret key for sites"
}
variable "rproxy_password" {
  type        = string
  sensitive   = true
  description = "Secret key for rproxy"
}
variable "monitoring_password" {
  type        = string
  sensitive   = true
  description = "Secret key for monitoring"
}
variable "nc_kumar_password" {
  type        = string
  sensitive   = true
  description = "Secret key for nc_kumar"
}
variable "plex_password" {
  type        = string
  sensitive   = true
  description = "Secret key for plex"
}
variable "vault_password" {
  type        = string
  sensitive   = true
  description = "Secret key for vault"
}
variable "torrents_password" {
  type        = string
  sensitive   = true
  description = "Secret key for torrents"
}
variable "med_dl_password" {
  type        = string
  sensitive   = true
  description = "Secret key for med_dl"
}
